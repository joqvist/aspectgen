/* Copyright (c) 2015, Jesper Öqvist <jesper.oqvist@cs.lth.se>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.jastadd.aspectgen;

import java.util.List;

import org.jastadd.aspectgen.AspectGenerator.AspectContext;
import org.jastadd.tinytemplate.Template;
import org.jastadd.tinytemplate.TemplateContext;
import org.jastadd.tinytemplate.fragment.Fragment;
import org.jastadd.tinytemplate.fragment.StringFragment;

public class AspectTemplate extends Template {

  public AspectTemplate() {
  }

  @Override
  protected void expandLine(TemplateContext context, List<Fragment> line, StringBuilder buf) {
    buf.setLength(0);
    if (line.size() == 1 && line.get(0).isNewline() && context instanceof AspectContext) {
      AspectContext con = (AspectContext) context;
      buf.append(con.ind());
      buf.append("out.println();\n");
    } else {
      boolean prevString = false;
      for (Fragment fragment : line) {
        if (prevString && fragment instanceof AspectNewline && context instanceof AspectContext) {
          AspectContext con = (AspectContext) context;
          buf.append(con.ind());
          buf.append("out.println();\n");
        } else {
          fragment.expand(context, buf);
        }
        prevString = fragment instanceof StringFragment;
      }
    }
  }
}
