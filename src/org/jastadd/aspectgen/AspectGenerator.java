/* Copyright (c) 2014-2015, Jesper Öqvist <jesper.oqvist@cs.lth.se>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.jastadd.aspectgen;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.ArrayList;

import org.jastadd.tinytemplate.FragmentBuilder;
import org.jastadd.tinytemplate.Indentation;
import org.jastadd.tinytemplate.Template;
import org.jastadd.tinytemplate.TemplateContext;
import org.jastadd.tinytemplate.TemplateParser;
import org.jastadd.tinytemplate.TinyTemplate;
import org.jastadd.tinytemplate.TemplateParser.SyntaxError;
import org.jastadd.tinytemplate.SimpleContext;
import org.jastadd.tinytemplate.fragment.AttributeReference;
import org.jastadd.tinytemplate.fragment.Conditional;
import org.jastadd.tinytemplate.fragment.Fragment;
import org.jastadd.tinytemplate.fragment.Include;
import org.jastadd.tinytemplate.fragment.Join;
import org.jastadd.tinytemplate.fragment.StringFragment;
import org.jastadd.tinytemplate.fragment.VariableReference;

/**
 * Generates a JastAdd aspect from templates for pretty printing.
 * @author Jesper Öqvist <jesper@llbit.se>
 */
public class AspectGenerator {

  private static final FragmentBuilder ASPECT_FRAGMENT_BUILDER = new FragmentBuilder() {

    @Override
    public VariableReference variable(String var) {
      return new AspectVariable(var);
    }

    @Override
    public Template template() {
      return new AspectTemplate();
    }

    @Override
    public StringFragment string(String theString) {
      return new AspectString(theString);
    }

    @Override
    public Include include(String template) {
      return new AspectInclude(template);
    }

    @Override
    public Conditional conditional(String condition, Template thenPart,
        Template elsePart) throws SyntaxError {
      return new AspectIf(condition, thenPart, elsePart);
    }

    @Override
    public Conditional conditional(String condition, Template thenPart)
      throws SyntaxError {
      return new AspectIf(condition, thenPart);
    }

    @Override
    public Join join(String iterable, String sep) throws SyntaxError {
      return new AspectJoin(iterable, sep);
    }

    @Override
    public AttributeReference attribute(String attr) {
      return new AspectAttribute(attr);
    }

    @Override
    public Fragment indentation(int levels) {
      return AspectIndentation.getFragment(levels);
    }

    @Override
    public Fragment newline() {
      return AspectNewline.INSTANCE;
    }
  };

  static class AspectContext extends TemplateContext {
    private final Indentation ind;
    private int level = 0;
    private String currentIndent = "";
    private final TemplateContext parent;

    public AspectContext(TemplateContext parent, String indentation) {
      ind = new Indentation(indentation);
      this.parent = parent;
    }

    @Override
    public Object evalVariable(String varName) {
      return parent.evalVariable(varName);
    }

    @Override
    public Object evalAttribute(String attrName) {
      return parent.evalAttribute(attrName);
    }

    @Override
    public String evalIndentation(int level) {
      return parent.evalIndentation(level);
    }

    @Override
    public void expand(TemplateContext tc, String templateName, PrintStream out) {
      parent.expand(tc, templateName, out);
    }

    @Override
    public void expand(TemplateContext tc, String templateName, PrintWriter out) {
      parent.expand(tc, templateName, out);
    }

    @Override
    public void expand(TemplateContext tc, String templateName, StringBuffer out) {
      parent.expand(tc, templateName, out);
    }

    @Override
    public void expand(TemplateContext tc, String templateName, StringBuilder out) {
      parent.expand(tc, templateName, out);
    }

    @Override
    public void bind(String varName, Object value) {
      throw new UnsupportedOperationException("Can not bind variable '" + varName
          + "' in aspect context.");
    }

    @Override
    public void flushVariables() {
      parent.flushVariables();
    }

    /**
     * @return current indentation string
     */
    public String ind() {
      return currentIndent;
    }

    /**
     * Increase indentation level
     */
    public void incInd() {
      level += 1;
      currentIndent = ind.get(level);
    }

    /**
     * Decrease indentation level
     */
    public void decInd() {
      level -= 1;
      currentIndent = ind.get(level);
    }

  }

  /**
   * Entry point for aspect generator
   * @param args
   */
  public static void main(String[] args) {
    Collection<String> ttFiles = new LinkedList<String>();
    String aspectName = "TemplateAspect";
    String indentation = "  ";
    for (int i = 0; i < args.length; ++i) {
      if (args[i].equals("-aspect")) {
        if (i + 1 < args.length) {
          aspectName = args[i + 1];
          i += 1;
        } else if (args[i].equals("-tab_indent")) {
          indentation = "\t";
        } else {
          throw new Error("missing argument to -aspect option");
        }
      } else {
        ttFiles.add(args[i]);
      }
    }

    TinyTemplate tt = new TinyTemplate();
    // Load all templates.
    for (String file : ttFiles) {
      try {
        tt.loadTemplates(new FileInputStream(file));
        TemplateParser parser = new TemplateParser(tt,
            new FileInputStream(file), ASPECT_FRAGMENT_BUILDER);
        parser.parse();
      } catch (FileNotFoundException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      } catch (SyntaxError e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }

    AspectContext context = new AspectContext(tt, indentation);

    if (tt.templates().contains("header")) {
      tt.expand(new SimpleContext(tt, indentation), "header", System.out);
    }
    System.out.println("aspect " + aspectName + " {");
    context.incInd();
    ArrayList<String> templateNames = new ArrayList<String>(tt.templates());
    Collections.sort(templateNames);
    for (String template : templateNames) {
      if (!template.equals("header")) {
        System.out.format("%spublic void %s.prettyPrint(PrettyPrinter out) {",
            context.ind(), template);
        System.out.println();
        context.incInd();
        tt.expand(context, template, System.out);
        context.decInd();
        System.out.println(context.ind() + "}");
      }
    }
    context.decInd();
    System.out.println("}");
  }
}
