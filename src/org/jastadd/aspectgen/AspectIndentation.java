/* Copyright (c) 2014, Jesper Öqvist <jesper.oqvist@cs.lth.se>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.jastadd.aspectgen;

import java.util.ArrayList;
import java.util.List;

import org.jastadd.aspectgen.AspectGenerator.AspectContext;
import org.jastadd.tinytemplate.TemplateContext;
import org.jastadd.tinytemplate.Indentation.IndentationFragment;
import org.jastadd.tinytemplate.fragment.Fragment;

public class AspectIndentation extends IndentationFragment {

  protected AspectIndentation(int indentLevel) {
    super(indentLevel);
  }

  @Override
  public void expand(TemplateContext context, StringBuilder out) {
    if (context instanceof AspectContext) {
      AspectContext con = (AspectContext) context;
      out.append(con.ind());
      out.append("out.indent(");
      out.append(""+level);
      out.append(");\n");
    } else {
      super.expand(context, out);
    }
  }

  private static final List<Fragment> fragments = new ArrayList<Fragment>(32);

  /**
   * @param level The level of indentation
   * @return An indentation fragment for the given indentation level
   */
  public static Fragment getFragment(int level) {
    while (fragments.size() < (level + 1)) {
      fragments.add(new AspectIndentation(fragments.size()));
    }
    return fragments.get(level);
  }
}
