/* Copyright (c) 2014, Jesper Öqvist <jesper.oqvist@cs.lth.se>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.jastadd.aspectgen;

import org.jastadd.aspectgen.AspectGenerator.AspectContext;
import org.jastadd.tinytemplate.TemplateContext;
import org.jastadd.tinytemplate.TemplateParser.SyntaxError;
import org.jastadd.tinytemplate.fragment.Join;

public class AspectJoin extends Join {

  public AspectJoin(String iterable, String sep) throws SyntaxError {
    super(iterable, sep);
  }

  @Override
  public void expand(TemplateContext context, StringBuilder out) {
    if (context instanceof AspectContext) {
      AspectContext con = (AspectContext) context;
      out.append(con.ind() + "out.join(");
      if (isAttribute) {
        out.append(iterable + "()");
      } else {
        out.append("get" + iterable + "()");
      }
      if (!sep.isEmpty()) {
        out.append(", new PrettyPrinter.Joiner() {\n");
        con.incInd();
        out.append(con.ind() + "@Override\n");
        out.append(con.ind() + "public void printSeparator(PrettyPrinter out) {\n");
        con.incInd();
        StringBuilder buf = new StringBuilder();
        for (int i = 0; i < sep.length(); ++i) {
          if ((i+1) < sep.length() && sep.charAt(i) == '\\' &&
              sep.charAt(i+1) == 'n') {
            i += 1;
            if (buf.length() > 0) {
              out.append(con.ind() + "out.print(\"" + buf.toString() + "\");\n");
            }
            out.append(con.ind() + "out.println();\n");
            buf.setLength(0);
          } else {
            buf.append(sep.charAt(i));
          }
        }
        if (buf.length() > 0) {
          out.append(con.ind() + "out.print(\"" + buf.toString() + "\");\n");
        }
        con.decInd();
        out.append(con.ind() + "}\n");
        con.decInd();
        out.append(con.ind() + "});\n");
      } else {
        out.append(");\n");
      }
    } else {
      super.expand(context, out);
    }
  }
}
